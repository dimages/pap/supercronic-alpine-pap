FROM registry.gitlab.com/affiliatelk/supercronic:latest

# https://github.com/codecasts/php-alpine
RUN apk add --update wget tar && \
    wget -O /etc/apk/keys/php-alpine.rsa.pub http://php.codecasts.rocks/php-alpine.rsa.pub && \
    echo "@php http://php.codecasts.rocks/v3.5/php-7.0" >> /etc/apk/repositories && \
    apk add --update php7

ADD crontab /root/platform-cron

# Ioncube loader
RUN wget --no-check-certificate https://downloads.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz && \
    tar -zxvf ioncube_loaders_lin_x86-64.tar.gz && \
    mkdir /usr/local/ioncube && \
    cp ioncube/ioncube_loader_lin_7.0.so /usr/local/ioncube/ioncube_loader_lin_7.0.so && \
    echo "zend_extension = /usr/local/ioncube/ioncube_loader_lin_7.0.so" >> /usr/local/etc/php/php.ini && \
    rm ioncube_loaders_lin_x86-64.tar.gz && \
    rm -R ioncube

WORKDIR /opt
ENTRYPOINT ["./supercronic"]
CMD ["./platform-cron"]